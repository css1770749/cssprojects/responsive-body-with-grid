let isMenuOpen = false;
function ToggleMenu() {
    isMenuOpen = !isMenuOpen;
    document.getElementById("menu-icon").innerHTML = isMenuOpen ? 'close' : 'menu';
    document.getElementById("menu").classList.toggle("display-block");
}
